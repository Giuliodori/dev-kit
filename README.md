A Personal tool to setup a Dev environment

```
        ▄▄                                       ▄▄
      ▀███                           ▀███        ██   ██
        ██                             ██             ██
   ▄█▀▀███   ▄▄█▀██▀██▀   ▀██▀         ██  ▄██▀▀███ ██████
 ▄██    ██  ▄█▀   ██ ██   ▄█           ██ ▄█     ██   ██
 ███    ██  ██▀▀▀▀▀▀  ██ ▄█     █████  ██▄██     ██   ██
 ▀██    ██  ██▄    ▄   ███             ██ ▀██▄   ██   ██
  ▀████▀███▄ ▀█████▀    █            ▄████▄ ██▄▄████▄ ▀████

█▓▒▒░░░ version 1.0.0░░░▒▒▓█
```


## Use

```
git clone git@gitlab.com:mlc-labs/dev-kit.git
cd dev-kit
source dev-kit
```

after that, you can check the use executing the command:

```
<$ dev-kit

Usage: dev-kit <tool> <command>

tools:
    dev-base
    docker
    latex

commands:
 install : install the tools

<$
```



## Resources

- https://github.com/alebcay/awesome-shell
- https://github.com/awesome-lists/awesome-bash

## License

This project is published under the term of the [MIT](./LICENSE).
