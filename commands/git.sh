#!/usr/bin/env bash

source utils/log4bash.sh


cmd__promt(){
  log_info "Installing magicmonty/bash-git-prompt...";
  git clone https://github.com/magicmonty/bash-git-prompt.git ~/.bash-git-prompt --depth=1

  if grep -q bash-git-prompt "$HOME/.bashrc"; then
    log_debug "git prompt already integrate to Bash "
  else
    log_debug "updating file $HOME/.bashrc "
    echo " " >> $HOME/.bashrc
    echo "# https://github.com/magicmonty/bash-git-prompt.git " >> $HOME/.bashrc
    echo "if [ -f \"\$HOME/.bash-git-prompt/gitprompt.sh\" ]; then" >> $HOME/.bashrc
    echo "  GIT_PROMPT_ONLY_IN_REPO=1" >> $HOME/.bashrc
    echo "  source $HOME/.bash-git-prompt/gitprompt.sh" >> $HOME/.bashrc
    echo "fi" >> $HOME/.bashrc
  fi
  log_success "Instalation ${YELLOW}magicmonty/bash-git-prompt${LOG_SUCCESS_COLOR} complete";
}
