#!/usr/bin/env bash
source utils/log4bash.sh

cmd__install(){
  log_info "Installing text editors...";
  log_info "Installing sublime text (stable version)...";
  wget -qO - https://download.sublimetext.com/sublimehq-pub.gpg | sudo apt-key add -
  sudo apt install apt-transport-https
  echo "deb https://download.sublimetext.com/ apt/stable/" | sudo tee /etc/apt/sources.list.d/sublime-text.list
  sudo apt update
  sudo apt install sublime-text
  log_info "Installing Atom...";
  wget -O /tmp/atom-amd64.deb https://atom.io/download/deb
  sudo dpki -i /tmp/atom-amd64.deb
  rm /tmp/atom-amd64.deb
  log_info "Installing Visual Code...";
  wget -O /tmp/visual-code.deb https://go.microsoft.com/fwlink/?LinkID=760868
  sudo dpki -i /tmp/visual-code.deb
  rm /tmp/visual-code.deb
  log_success "Instalation ${YELLOW}sublime, atom, visual-code${LOG_SUCCESS_COLOR} complete";
}
