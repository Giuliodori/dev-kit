#!/usr/bin/env bash
source utils/log4bash.sh

cmd__install(){
  log_info "Installing xetex...";
  local packages="texlive-xetex texlive-luatex texlive-lang-spanish texlive-lang-french texlive-lang-english texlive-fonts-recommended texlive-fonts-extra  "
  sudo apt install ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}
