#!/usr/bin/env bash
source utils/log4bash.sh

cmd__install(){
  log_info "Installing docker ce...";
  local packages="docker-ce"
  sudo apt -y install apt-transport-https ca-certificates software-properties-common
  curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
  sudo apt-key fingerprint 0EBFCD88
  sudo add-apt-repository \
     "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
     $(lsb_release -cs) \
     stable"
  sudo apt update
  sudo apt -y install ${packages}
  ## https://docs.docker.com/install/linux/linux-postinstall/#manage-docker-as-a-non-root-user
  log_success "updating docker user";
  sudo groupadd docker
  sudo usermod -aG docker $USER
  docker run hello-world
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
  #
  # Docker Compose
  #
  log_info "Installing docker-compose...";
  sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
  sudo chmod +x /usr/local/bin/docker-compose
  log_success "Instalation ${YELLOW}docker-compose${LOG_SUCCESS_COLOR} complete";
}
