#!/usr/bin/env bash


source utils/log4bash.sh

cmd__install(){
  log_info "Installing xetex...";
  local packages="git make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev xz-utils tk-dev libxml2-dev libxmlsec1-dev libffi-dev liblzma-dev "
  sudo apt install --no-install-recommends ${packages}
  log_success "Instalation ${YELLOW}${packages}${LOG_SUCCESS_COLOR} complete";
}
